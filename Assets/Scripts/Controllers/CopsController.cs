﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CopsController : MonoBehaviour
{
    
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
 
    //Default value in comment
    public float maximumLookDistance;//30
    public float maximumAttackDistance;//10
    public float minimumDistanceFromPlayer;//2
 
    public float rotationDamping;//2
 
    public float shotInterval;//0.5
 
    public float shotTime;//0

    private GameObject[] players;
    private GameObject closestPlayer;
    
    // Start is called before the first frame update
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        closestPlayer = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (players.Length == 0)
        {
            players = GameObject.FindGameObjectsWithTag("Player");
        }
        closestPlayer = players.OrderBy(go => Vector3.Distance(go.transform.position, transform.position))
                                .FirstOrDefault();
        if (closestPlayer)
        {
            var distance = Vector3.Distance(closestPlayer.transform.position, transform.position);
            if(distance <= maximumLookDistance)
            {
                LookAtTarget();
                //Check distance and time
                if(distance <= maximumAttackDistance && (Time.time - shotTime) > shotInterval)
                {
                    Shoot();
                }
            }   
        }   
    }
 
    private void LookAtTarget()
    {
        var dir = closestPlayer.transform.position - transform.position;
        //dir.y = 0;
        var rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * rotationDamping);
    }
 
 
    private void Shoot()
    {
        //Reset the time when we shoot
        shotTime = Time.time;
        //transform.position + (target.position - transform.position).normalized, Quaternion.LookRotation(target.position - transform.position)
        var bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;
        Destroy(bullet, 2.0f);
    }
}
