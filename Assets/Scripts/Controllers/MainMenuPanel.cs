using System.Collections;
using UnityEngine;

namespace Controllers
{
    public class MainMenuPanel : MonoBehaviour
    {
        private int _selector = -1;
        private bool _canInteract = true;
        private GameObject[] _buttons = {};
        public AudioSource MusicButton;
        public AudioSource MusicClick;

        private void Start()
        {
            UpdateButtons();
        }

        private void Update()
        {
            if (!gameObject.activeSelf) return;

            HandleVerticalInput();
            HandlePressButtonsEvents();
        }

        private void OnEnable()
        {
            UpdateButtons();
            StartCoroutine(MenuChange());
        }

        private void HandlePressButtonsEvents()
        {
            if (Input.GetButton("Fire2") && _canInteract)
            {
                MusicClick.Play();
                _canInteract = false;
                int maxBtns = _buttons.Length;
                if (maxBtns > 0 && _selector >= 0 && _buttons[_selector] != null)
                {
                    UnityEngine.UI.Button btn = _buttons[_selector].GetComponent<UnityEngine.UI.Button>();
                    if (btn != null) btn.onClick.Invoke();
                }

                UpdateButtons();
                if (gameObject.activeSelf) StartCoroutine(MenuChange());
            }
        }

        private void HandleVerticalInput()
        {
            int verticalInput = -(int)Input.GetAxis("Vertical");

            int maxBtns = _buttons.Length;
            if (verticalInput != 0 && _canInteract && maxBtns > 0)
            {
                _canInteract = false;
                if (_selector + verticalInput < 0)
                {
                    _selector = maxBtns - 1;
                } else if (_selector + verticalInput >= maxBtns)
                {
                    _selector = 0;
                } else
                {
                    _selector += verticalInput;
                }

                StartCoroutine(MenuChange());
            }

            if (maxBtns > 0 && _selector >= 0 && _buttons[_selector] != null)
            {
                UnityEngine.UI.Button btn = _buttons[_selector].GetComponent<UnityEngine.UI.Button>();
                if (btn != null) btn.Select();
            }
        }

        private void UpdateButtons()
        {
            _buttons = GameObject.FindGameObjectsWithTag("MainMenuBtn");
        }

        IEnumerator MenuChange()
        {
            MusicButton.Play();
            yield return new WaitForSeconds(0.3f);
            _canInteract = true;
        }
    }
}
