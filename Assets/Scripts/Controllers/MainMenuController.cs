﻿using Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class MainMenuController : MonoBehaviour
    {
        public GameObject PauseMenu;

        public void ContinueGame()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.lockState = CursorLockMode.Locked;
            GameManager.Instance.ActiveTime();
            PauseMenu.gameObject.SetActive(false);
            GameManager.Instance.GameIsInPause = false;
        }

        public void SelectNumberOfPlayersMenu()
        {
            var mainMenuPanel = GetMainMenuPanelFromBtn();
            var playGamePanel = GetPlayGamePanelFromBtn();
            mainMenuPanel.SetActive(false);
            playGamePanel.SetActive(true);
        }

        public void BackToMainMenuFromPlayGamePanel()
        {
            var mainMenuPanel = GetMainMenuPanelFromBtn();
            var playGamePanel = GetPlayGamePanelFromBtn();
            playGamePanel.SetActive(false);
            mainMenuPanel.SetActive(true);
        }

        public void SelectSettingsMenu()
        {
            var mainMenuPanel = GetMainMenuPanelFromBtn();
            var settingsPanel = GetSettingsPanelFromBtn();
            mainMenuPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }

        public void BackToMainMenuFromSettingsPanel()
        {
            var mainMenuPanel = GetMainMenuPanelFromBtn();
            var settingsPanel = GetSettingsPanelFromBtn();
            settingsPanel.SetActive(false);
            mainMenuPanel.SetActive(true);
        }

        public void PlayWith1Player()
        {
            PlayGame(1);
        }

        public void PlayWith2Player()
        {
            PlayGame(2);
        }

        public void PlayWith3Player()
        {
            PlayGame(3);
        }

        public void PlayWith4Player()
        {
            PlayGame(4);
        }

        public void GoToMainMenu()
        {
            Cursor.visible = true;
            SceneManager.LoadScene(0);
        }

        public void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
		#endif
        }

        private GameObject GetMainMenuPanelFromBtn()
        {
            Transform mainMenuRoot = transform.parent.transform.parent;
            return mainMenuRoot.Find("MainMenuPanel").gameObject;
        }

        private GameObject GetPlayGamePanelFromBtn()
        {
            Transform mainMenuRoot = transform.parent.transform.parent;
            return mainMenuRoot.Find("PlayGamePanel").gameObject;
        }

        private GameObject GetSettingsPanelFromBtn()
        {
            Transform mainMenuRoot = transform.parent.transform.parent;
            return mainMenuRoot.Find("SettingsPanel").gameObject;
        }

        private void PlayGame(uint nbPlayers)
        {
            GameManager.Instance.NbPlayers = nbPlayers;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            GameManager.Instance.ActiveTime();
            GameManager.Instance.GameIsInPause = false;
            SceneManager.LoadScene(1);
        }
    }
}
