﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float MoveSpeed;
    public int PlayerNumber;
    public LayerMask groundLayer;
    public Transform feetPosition;

    private Animator _Animator;
    private Rigidbody m_rigidBody;

    private float m_walkScale = 0.5f;
    private float m_saveWalkScale;
    private readonly float m_interpolation = 10;

    private float m_currentV = 0;
    private float m_currentH = 0;
    private Vector3 m_currentDirection = Vector3.zero;

    private float m_jumpTimeStamp = 0;
    private float m_minJumpInterval = 0.5f;
    private float m_jumpForce = 4;

    private float timeBlinks;
    private bool isHitted;

    public AudioSource MusicJump;

    // Start is called before the first frame update
    void Start()
    {
        m_saveWalkScale = m_walkScale;
        timeBlinks = 1;
        isHitted = false;
        m_rigidBody = GetComponent<Rigidbody>();
        _Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.GameIsInPause) return;
        
        _Animator.SetBool("Grounded", true);
        MoveUpdate();
    }

    IEnumerator DoBlinks(float duration, float blinkTime)
    {
        Renderer[] allRenders = gameObject.GetComponentsInChildren<Renderer>();

        m_walkScale = 1.1f;
        while (duration > 0f)
        {
            duration -= Time.deltaTime;

            //toggle renderer
            foreach (Renderer render in allRenders)
            {
                render.enabled = !render.enabled;
            }

            //wait for a bit
            yield return new WaitForSeconds(blinkTime);
        }

        //make sure renderer is enabled when we exit
        foreach (Renderer render in allRenders)
        {
            render.enabled = true;
        }

        m_walkScale = m_saveWalkScale;
        isHitted = false;
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("FinishLine"))
        {
            Application.LoadLevel("EndGameScene");
        }
    }

    private void Hitted()
    {
        if (!isHitted)
        {
            isHitted = true;            
            StartCoroutine(DoBlinks(timeBlinks, 0.2f));
        }
    }

    private void MoveUpdate()
    {
        float v = Input.GetAxis("player_" + PlayerNumber + "_axis_v");
        float h = Input.GetAxis("player_" + PlayerNumber + "_axis_h");

        Transform camera = Camera.main.transform;

        if (Input.GetButton("player_" + PlayerNumber + "_R2"))
        {
            v /= m_walkScale;
            h /= m_walkScale;
        }

        m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);
        m_currentH = Mathf.Lerp(m_currentH, h, Time.deltaTime * m_interpolation);

        Vector3 direction = camera.forward * m_currentV + camera.right * m_currentH;

        float directionLength = direction.magnitude;
        direction.y = 0;
        direction = direction.normalized * directionLength;

        if(direction != Vector3.zero)
        {
            m_currentDirection = Vector3.Slerp(m_currentDirection, direction, Time.deltaTime * m_interpolation);

            transform.rotation = Quaternion.LookRotation(m_currentDirection);
            transform.position += m_currentDirection * MoveSpeed * Time.deltaTime;

             _Animator.SetFloat("MoveSpeed", direction.magnitude);
        }
        bool isGrounded = Physics.CheckSphere(feetPosition.position, 0.3f, groundLayer, QueryTriggerInteraction.Ignore);
        if (isGrounded)
        {
            JumpingAndLanding();   
        }
    }

    private void JumpingAndLanding()
    {
        bool jumpCooldownOver = (Time.time - m_jumpTimeStamp) >= m_minJumpInterval;

        if (jumpCooldownOver && Input.GetButton("player_" + PlayerNumber + "_X"))
        {
            MusicJump.Play();
            m_jumpTimeStamp = Time.time;
            if (m_rigidBody)
            {
                m_rigidBody.AddForce(Vector3.up * m_jumpForce, ForceMode.Impulse);
            }
        }
    }
}
