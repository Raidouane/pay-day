﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public float speed;
    
    private bool isTriggered;
    
    void Start()
    {
        isTriggered = false;
    }
    
    void Update()
    {
        if (isTriggered)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                new Vector3(10.6f, 0.05f, -10f),
                Time.deltaTime * speed);   
        }
    }

    private void Forward()
    {
        isTriggered = true;
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SendMessage("Hitted");
        }
    }
}
