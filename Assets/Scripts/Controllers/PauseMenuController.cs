using System;
using System.Collections;
using Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class PauseMenuController : MonoBehaviour
    {
        public GameObject BackToMainMenuBtn;
        public GameObject ContinueGameBtn;
        private int _selector = -1;
        private bool _canInteract = true;
        private GameObject[] _buttons = {};
        private GameObject _menuPause = null;

        private void Start()
        {
            _menuPause = transform.Find("MenuPause").gameObject;
            _buttons = new[]
            {
                ContinueGameBtn,
                BackToMainMenuBtn
            };
        }

        private void Update()
        {
            HandlePressButtonsEvents();
            HandleVerticalInput();
        }

        private void OnEnable()
        {
            StartCoroutine(MenuChange());
        }

        public void GoToMainMenu()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = true;
            SceneManager.LoadScene(0);
        }

        private void HandlePressButtonsEvents()
        {
            if (Input.GetKeyDown("joystick button 9"))
            {
                HandleOpeningPauseMenu();
            }


            if (!Input.GetButton("Fire2") || !GameManager.Instance.GameIsInPause) return;
            _canInteract = false;
            int maxBtns = _buttons.Length;
            if (maxBtns > 0 && _selector >= 0 && _buttons[_selector] != null)
            {
                UnityEngine.UI.Button btn = _buttons[_selector].GetComponent<UnityEngine.UI.Button>();
                _canInteract = true;
                if (btn != null) btn.onClick.Invoke();
            }
        }

        private void HandleOpeningPauseMenu()
        {
            if (_menuPause == null) return;

            bool isInPause = _menuPause.activeSelf;
            Debug.Log("PAUSE" + isInPause);
            GameManager.Instance.GameIsInPause = !isInPause;
            _menuPause.SetActive(!isInPause);
        }

        private void HandleVerticalInput()
        {
            int verticalInput = -(int)Input.GetAxis("Vertical");

            int maxBtns = _buttons.Length;
            if (verticalInput != 0 && _canInteract && maxBtns > 0)
            {
                _canInteract = false;
                if (_selector + verticalInput < 0)
                {
                    _selector = maxBtns - 1;
                } else if (_selector + verticalInput >= maxBtns)
                {
                    _selector = 0;
                } else
                {
                    _selector += verticalInput;
                }

                StartCoroutine(MenuChange());
            }

            if (maxBtns > 0 && _selector >= 0 && _buttons[_selector] != null)
            {
                UnityEngine.UI.Button btn = _buttons[_selector].GetComponent<UnityEngine.UI.Button>();
                if (btn != null) btn.Select();
            }
        }


        IEnumerator MenuChange()
        {
            yield return new WaitForSeconds(0.3f);
            _canInteract = true;
        }
    }
}
