using Managers;
using UnityEngine;

namespace Controllers
{
    public class SoundButtonsFromSettingsMenuController : MonoBehaviour
    {
        public GameObject EnableSoundButton;
        public GameObject DisableSoundButton;

        private void Start()
        {
            if (GameManager.Instance.SoundIsEnabled)
            {
                ActiveDisableSoundBtn();
                return;
            }

            ActiveEnableSoundBtn();
        }

        public void HandleEnableSound()
        {
            GameManager.Instance.SoundIsEnabled = true;
            ActiveDisableSoundBtn();
        }

        public void HandleDisableSound()
        {
            GameManager.Instance.SoundIsEnabled = false;
            ActiveEnableSoundBtn();
        }

        private void ActiveEnableSoundBtn()
        {
            EnableSoundButton.SetActive(true);
            DisableSoundButton.SetActive(false);
        }

        private void ActiveDisableSoundBtn()
        {
            EnableSoundButton.SetActive(false);
            DisableSoundButton.SetActive(true);
        }
    }
}
