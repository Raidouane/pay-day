﻿using Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class PauseMenuActions : MonoBehaviour
    {
        private GameObject MenuPause;

        public void ContinueGame()
        {
            MenuPause.SetActive(false);
        }
    }
}
