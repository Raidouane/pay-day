﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisions : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject);
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SendMessage("Hitted");
        }
    }
}
