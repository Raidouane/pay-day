using UnityEngine;

namespace Managers
{
    public class GameManager
    {
        #region Public Section

        public static GameManager Instance
        {
            get { return _instance ?? (_instance = new GameManager()); }
        }

        public uint NbPlayers
        {
            get { return _nbPlayers; }
            set { _nbPlayers = value; }
        }

        public bool SoundIsEnabled
        {
            get { return _soundIsEnabled; }
            set { _soundIsEnabled = value; }
        }

        public bool GameIsInPause
        {
            get { return _gameIsInPause; }
            set { _gameIsInPause = value; }
        }

        public void ActiveTime()
        {
            Time.timeScale = 1;
        }

        #endregion

        #region Private Section

        private static GameManager     _instance;
        private uint                   _nbPlayers = 1;
        private bool                   _soundIsEnabled = true;
        private bool                   _gameIsInPause = false;

        private GameManager() {}

        #endregion
    }
}
