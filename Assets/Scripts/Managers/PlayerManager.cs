﻿using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEditor;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject prefabP1;
    public GameObject prefabP2;
    public GameObject prefabP3;
    public GameObject prefabP4;
    public Camera cam;
    float xMax = 0f;
    int nbPlayer = 0;
    
    private GameObject[] _players;
    
    // Start is called before the first frame update
    void Start()
    {
        _players = new GameObject[GameManager.Instance.NbPlayers];
        for (int i = 0; i < GameManager.Instance.NbPlayers; i++)
        {
            switch (i)
            {
                case 0:
                     _players[i] = Instantiate(
                         prefabP1,
                         new Vector3(-10.20f, 0.5f, 2.29f),
                         Quaternion.EulerAngles(new Vector3(0f, 90f, 0f))
                         );
                    break;
                case 1:
                    _players[i] = Instantiate(
                        prefabP2,
                        new Vector3(-10.20f, 0.5f, 0.89f),
                        Quaternion.EulerAngles(new Vector3(0f, 90f, 0f))
                        );
                    break;
                case 2:
                    _players[i] = Instantiate(
                        prefabP3,
                        new Vector3(-10.20f, 0.5f, -0.59f),
                        Quaternion.EulerAngles(new Vector3(0f, 90f, 0f))
                        );
                    break;
                case 3:
                    _players[i] = Instantiate(
                        prefabP4,
                        new Vector3(-10.20f, 0.5f, -2.09f),
                        Quaternion.EulerAngles(new Vector3(0f, 90f, 0f))
                        );
                    break;
                default:
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < GameManager.Instance.NbPlayers; i++)
        {
            bool isUpper = false;
            if (_players[i].transform.position.x > xMax)
            {
                xMax = _players[i].transform.position.x;
                nbPlayer = i;
                isUpper = true;
            }
            if (!isUpper)
            {
                xMax = _players[nbPlayer].transform.position.x;
            }
        }
        Vector3 pos = cam.transform.position;
        pos.x = xMax;
        cam.transform.position = pos;
    }
}
